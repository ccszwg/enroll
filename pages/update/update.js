const util = require('../../utils/util.js')
const app = getApp()
Page({
  data: {
    index: 0,
    startdate: '2019-09-01',
    starttime: '12:01:00',
    enddate: '2019-09-01',
    endtime: '13:01',
    imgpath: '../../imgs/add.png'
  },
  onLoad: function (options) {
    this.setTime();
  },
  onShow: function () {

  },

  bindDateStart: function (e) {
    console.log(e);
    this.setData({
      startdate: e.detail.value
    })
    this.setTime();
    this.checkTime();
  },
  bindTimeStart: function (e) {
    var time = e.detail.value + ':00'
    this.setData({
      starttime: time
    })
    this.setTime();
    this.checkTime();
  },

  bindDateEnd: function (e) {
    this.setData({
      enddate: e.detail.value
    })
    this.checkTime();
  },
  bindTimeEnd: function (e) {
    var time = e.detail.value + ':00'
    this.setData({
      endtime: time
    })
    this.checkTime();

  },
  setTime: function () {
    //获取开始时间的毫秒数
    var start = this.data.startdate + ' ' + this.data.starttime
    var formatstart = new Date(start).getTime();

    //再根据后台用户类型活动区间天数相加
    var formatEndTime = formatstart + app.globalData.quTime * 86400000;
    //把结束时间转换成日期和时间,并将/转换成-
    var enddate = util.formatTime(formatEndTime, 'Y/M/D').split('/').join('-')
    this.setData({
      enddate: enddate.split(' ')[0],
      endtime: enddate.split(' ')[1]
    })
  },
  checkTime: function () {
    var end = this.data.enddate + ' ' + this.data.endtime
    var formatEnd = new Date(end).getTime();

    var start = this.data.startdate + ' ' + this.data.starttime
    var formatstart = new Date(start).getTime();
    if (formatstart > formatEnd) {
      this.setTime();
      wx.showToast({
        title: '开始时间不能大于结束时间',
        icon: 'none',
        duration: 2000
      })
    }
  },
  getImg: function () {
    var _this = this;
    //选择图片
    wx.chooseImage({
      success: function (res) {
        console.log(res);
        //预览显示
        _this.setData({
          imgpath: res.tempFilePaths[0]
        });
      },
    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})