## 平台简介
实现一个微信报名小程序，提升自己前端能力，并分享给有需要的朋友，有需要使用微信小程序做报名项目可以拿来借鉴一下。

## 演示图
<table>
    <tr>
        <td><img src="http://cloud.shenkk.com/00680OHJly1g9ja78jjkij30k01d6ju6.jpg"/></td>
        <td><img src="http://cloud.shenkk.com/00680OHJly1g9ja78io8qj30k01k8q3v.jpg"/></td>
    </tr>
    <tr>
        <td><img src="http://cloud.shenkk.com/00680OHJly1g9ja78myc1j30k01ci77h.jpg"/></td>
        <td><img src="http://cloud.shenkk.com/00680OHJly1g9ja78io8qj30k01k8q3v.jpg"/></td>
    </tr>


</table>

## QQ交流群
640049288
